# SurfacePenActivator

Customized Surface Pen Button

## Prerequisities

AutoHotkey (https://autohotkey.com/)

## How to install

1. Just download `SurfacePenActivator.ahk`
2. Open Run Dialog (Press `Win+R`)
3. Type `shell:startup`
4. Make shortcut to `SurfacePenActivator.ahk` in startup application folder

## Current Operations

### Powerpoint mode
* Single click : Next page
* Double click : Start presentation from current slide
* Long press : Mode change (Pen mode or Laser point mode)

### Normal mode
* Single click : Find selected word (by edge)
* Double click : Open OneNote (Add new page when OneNote is opened)
* Long press : Windows key

## Tips

- #F20 : single click
- #F19 : double click
- #F18 : long press

## Reference

* https://www.reddit.com/r/Surface/comments/43ohn6/another_sp4_pen_top_button_autohotkey_script/
