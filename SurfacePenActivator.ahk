; Single click
#F20::
    IfWinActive ahk_exe Powerpnt.exe
    {
        Send {Right}
        return
    }
    Send, {Ctrl down}c{Ctrl up}
    StringSplit, words, Clipboard, ' '
    if StrLen(words1) < StrLen(words2)
    {
        word = %words2%
    }
    else
    {
        word = %words1%
    }
;    Run % "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe http://dic.daum.net/search.do?dic=eng&q=" . word
    Run % "microsoft-edge:http://dic.daum.net/search.do?dic=eng&q=" . word
Return

; Double click
#F19::
    IfWinActive ahk_exe Powerpnt.exe
    {
        Send {Shift down}{F5}{Shift up}
        return
    }

    IfWinNotExist ahk_exe OneNote.exe
    {
        Run C:\Program Files\Microsoft Office\Office16\OneNote.exe
        return
    }
    IfWinActive ahk_exe OneNote.exe
    {
        Send, ^n
        return
    }
    IfWinExist ahk_exe OneNote.exe
    {
        WinActivate
        return
    }
Return


; Long press
#F18::
    IfWinActive ahk_exe Powerpnt.exe
    {
        if (laser := !laser)
        {
            Send, {Ctrl down}l{Ctrl up}
        }
        else
        {
            Send, {Ctrl down}p{Ctrl up}
        }
        return
    }
    Send, {LWin}
Return
